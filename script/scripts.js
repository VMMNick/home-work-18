function deepClone(obj) {
    // Перевірка, чи obj є об'єктом або масивом
    if (typeof obj !== 'object' || obj === null) {
      return obj; // Якщо obj не об'єкт, повертаємо його без змін
    }
  
    // Створюємо новий об'єкт або масив, залежно від типу obj
    const clone = Array.isArray(obj) ? [] : {};
  
    // Рекурсивно клонуємо всі властивості obj
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        clone[key] = deepClone(obj[key]);
      }
    }
  
    return clone;
  }
  
  // Приклад використання функції
  const originalObj = {
    name: 'John',
    age: 30,
    hobbies: ['reading', 'hiking'],
    address: {
      street: '123 Main St',
      city: 'Anytown',
    },
  };
  
  const clonedObj = deepClone(originalObj);
  console.log(clonedObj);
  